var config;

if (!config) {
    require('dotenv').config({
        path: process.env.NODE_ENV === 'alt_dev' ? './alt_dev.env' : '/usr/local/bin/environment.env'
    })
    config = {
        services: {
            secret: process.env.services_secret,
            scheduler: process.env.services_scheduler,
            broadcast: process.env.services_broadcast,
            tickets: process.env.services_tickets,
        },
        'baseUrl': process.env.baseUrl,
        'FBAppID': process.env.FBAppID,
        'FBAppSecret': process.env.FBAppSecret,
        'secretKey': process.env.secretKey,
        'mongoUrl' : process.env.mongoUrl,
		'googlePublicKey': process.env.googlePublicKey,
		'googleAccToken': process.env.googleAccToken,
        'googleRefToken': process.env.googleRefToken,
        'googleClientID': process.env.googleClientID,
        'googleClientSecret': process.env.googleClientSecret,
        'applePassword': process.env.applePassword,
        'facebookMessengerVerifyToken' : process.env.facebookMessengerVerifyToken,
        'facebookAccountLinkingAuthCode': process.env.facebookAccountLinkingAuthCode,
        'gcmApiKey': process.env.gcmApiKey,
        'mailchimpApiKey': process.env.mailchimpApiKey,
        'mailgunApiKey': process.env.mailgunApiKey,
        'mailgunBaseUrl': process.env.mailgunBaseUrl,
        'mailgunDomain': process.env.mailgunDomain,
        'awsS3Region': process.env.awsS3Region,
        'awsS3Bucket': process.env.awsS3Bucket,
        'awsS3AccessKey': process.env.awsS3AccessKey,
        'awsS3Secret': process.env.awsS3Secret,
        'awsS3Url': process.env.awsS3Url,
        'stripeApiSecretKey': process.env.stripeApiSecretKey,
        'stripeClientID': process.env.stripeClientID, // for oauth
        'slackWebHookUrl': process.env.slackWebHookUrl,
        'slack': {
            'client_id': process.env.slack_client_id,
            'client_secret': process.env.slack_client_secret,
            'token': process.env.slack_token,
            'outgoing_token': process.env.slack_outgoing_token
        },
        'paypal': {
            'mode': process.env.paypal_mode, // set "live" for production
            'baseUrl': process.env.paypal_baseUrl,
            'appId': process.env.paypal_appId,
            'version': process.env.paypal_version,
            'userId': process.env.paypal_userId,
            'password': process.env.paypal_password,
            'signature': process.env.paypal_signature,
            'grantPermissionsUrls': {
                'live': process.env.paypal_grantPermissionsUrls_live,
                'sandbox': process.env.paypal_grantPermissionsUrls_sandbox
            },
            'apiHosts': {
                'live': process.env.paypal_apiHosts_live,
                'sandbox': process.env.paypal_apiHosts_sandbox
            }
        }
    }
    config.currency_symbols = {
        'SGD': '$',
        'THB': '฿',
        'MYR': 'RM',
        'IDR': 'Rp',
        'PHP': '₱',
        'MMK': 'K',
        'VND': '₫',
        'KHR': '៛',
        'INR': '₹',
        'LKR': 'Rs.',
    }
}

module.exports = config;
